# Produção e Logística

Modelos, métodos e algoritmos aplicados a problemas integrados de produção e logística.

O curso tem como objetivo promover o entendimento dos desafios e vantagens no desenvolvimento de modelos matemáticos integrados (capazes de lidar com toda ou parte da cadeia de produção de um determinado setor).

Tópicos de otimização variados serão introduzidos para capacitar o estudante no entendimento destes modelos e nas formas de solução.

Ao final do curso, o aluno deverá:
1. demonstrar conhecimento e entendimento de como é feita e modelagem matemática para a integração de problemas de planejmanto da produção, sequenciamento, alocação e roteamento em diversos setores da indústria.
2. implementar modelos matemáticos integrados em pacotes de otimização.
3. compreender como que técnicas de decomposição podem ser empregadas para reduzir a complexidade dos modelos.
