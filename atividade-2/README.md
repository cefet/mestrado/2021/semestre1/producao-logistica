# Modelos, Métodos e Algoritmos aplicados a problemas integrados de produção e logística

Disciplina do Programa de Pós-Graduação em Modelagem Matemática e Computacional (PPGMMC) do Centro Federal de Educação Tecnológica de Minas Gerais CEFET-MG.

## Atividade Prática 2

Uso do resolvedor Gurobi em Python.

Problema de corte de estoque unidimensional com geração de colunas.

### 0. Preparando o ambiente

No terminal:

* venv

    Criar ambiente virtual
    ~~~sh
    python3 -m venv .venv
    ~~~

    Ativar o ambiente virtual
    ~~~sh
    source .venv/bin/activate
    ~~~

* poetry
    Criar ambiente virtual, se necessário
    ~~~sh
    poetry init
    ~~~

    Ativar o ambiente virtual
    ~~~sh
    poetry shell
    ~~~

### 1. Instalação e Configuração Gurobi e Python

Caso as dependências do projeto já estejam definidas:

~~~sh
pip install --requirement requirements.txt
~~~

ou

~~~sh
poetry install
~~~

### 2. Resolvendo um problema no Gurobi

Modelo simples de mix de produção:

Objetivo é maximizar o lucro total, definindo quantas unidades $x_j$ serão produzidas de cada produto $j \in \{1, 2, 3\}$. Cada unidade  produto tem um tempo de uso das máquinas e as máquinas tem um tempo de uso total.

Modelagem:
```math
\begin{equation*}
    \begin{aligned}
        & \underset{x_1, x_2, x_3}{\text{maxmizar}}
        & & 12 x_1 + 18 x_2 + 22 x_3 \\
        & \text{sujeito a} & &  1,5 x_1 + 1,2 x_3 \leq 120 \\
        & & &  2,2 x_2 + 2,0 x_3 \leq 200 \\
        & & &  1,2 x_1 + 1,4 x_2 + 2,4 x_3 \leq 250 \\
        & & &  x_j \geq 0, \; \forall j \in \{1, 2, 3\}. \\
    \end{aligned}
\end{equation*}
```

Para rodar o código:
~~~sh
python mix_producao.py
~~~

### 3. Problema da Mochila (Gurobi e Python)

Modelagem:
```math
\begin{equation*}
    \begin{aligned}
        & {\text{maxmizar}} & & \sum_{j=1}^{J} v_j \cdot x_j  \\
        & \textrm{Subject to: } & & \sum_{j=1}^{J} p_j \cdot x_j \leq c  \\
        & & &  x_j \in \{0,1\} \,\,\, \forall 1 \leq j \leq J
    \end{aligned}
\end{equation*}
```

Para rodar o código:
~~~sh
python problema_mochila.py
~~~

### 4. Geração de Colunas
Problema de corte de estoque unidimensional

Para rodar o código:
~~~sh
python problema_corte_1d.py
~~~
