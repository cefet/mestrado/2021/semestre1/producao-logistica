"""
Problema de mix de produção

Objetivo é maximizar o lucro total, definindo quantas unidades $x_j$
serão produzidas de cada produto $j \in \{1, 2, 3\}$.
Cada unidade  produto tem um tempo de uso das máquinas e
as máquinas tem um tempo de uso total.
"""

import numpy as np
import gurobipy as gp


def modelagem(
    lucros: np.ndarray,
    usos: np.ndarray,
    disponibilidades: np.ndarray,
    nome="Mix de Produção",
) -> gp.Model:
    """
    Define a modelagem do problema.

    Adiciona variáveis de decisão e restrições,
    além de definir a função objetivo.

    Args:
        lucros (np.ndarray): Lucros por produtos.
        usos (np.ndarray): Tempo de uso por produto (linha)
            e por máquina (coluna).
        disponibilidades (np.ndarray): Disponibilidade total por máquinas.
        nome (str, optional): Nome do modelo. Por padrão: "Mix de Produção".

    Returns:
        gp.Model: Modelo configurado.
    """
    modelo = gp.Model(nome)

    total_produtos = len(lucros)
    quantidades = modelo.addMVar(total_produtos)

    modelo.setObjective(lucros.T @ quantidades, sense=gp.GRB.MAXIMIZE)

    restricoes = modelo.addMConstr(usos, quantidades, "<=", disponibilidades)

    return modelo


def resultados(modelo: gp.Model, disponibilidades: np.ndarray) -> np.ndarray:
    """
    Calcula a ocupação das máquinas em porcentagem.

    Args:
        modelo (gp.Model): Modelo de otimização.
        disponibilidades (np.ndarray): Disponibilidade total por máquinas.

    Returns:
        np.ndarray: Ocupação percentual das máquinas.
    """
    ociosidade = modelo.Slack
    ocupacao = (disponibilidades - ociosidade) / disponibilidades * 100

    return ocupacao


def exibir(modelo: gp.Model, ocupacao: np.ndarray):

    produtos = modelo.X
    objetivo = modelo.objVal
    nome = modelo.ModelName

    print(f"\n\nProblema {nome}\n")
    print("Modelagem:")
    modelo.display()

    print(f"\nResultados:")

    print("Produzir")
    for produto, unidades in enumerate(produtos):
        print(f"{unidades} unidades do produto {produto}")

    print("\nUsar")
    for maquina, tempo in enumerate(ocupacao):
        print(f"{tempo} % da máquina {maquina}")

    print(f"\nAtingir um lucro total de: {objetivo}")


def principal():
    """
    Define os dados de entrada, chamas as funções de processamento
    e exibi o resultado.
    """

    # Lucro por produto
    lucros = np.array([12, 18, 22])

    # Tempo de uso por produto (linha) e por máquina (coluna)
    usos = np.array([[1.5, 0.0, 1.2], [0.0, 2.2, 1.4], [1.2, 2.0, 2.4]])

    # Disponibilidade total por máquinas
    disponibilidades = np.array([120, 200, 250])

    modelo = modelagem(lucros, usos, disponibilidades)

    modelo.optimize()

    ocupacao = resultados(modelo, disponibilidades)

    exibir(modelo, ocupacao)


if __name__ == "__main__":
    principal()
