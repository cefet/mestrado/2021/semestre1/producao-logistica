"""
Problema da mochila 0/1

Dados alguns itens distrintos com valor e peso específicos,
e uma mochila de capacidade máxima.

O objetivo é selecionar itens para colocar na mochila para
obter o maior valor, respeitando a capacidade da mochila.

Os itens apresentam somente dois estados:
selecionado "1" ou não selecionado "0".
"""

from dataclasses import dataclass
import numpy as np
import gurobipy as gp


@dataclass
class Corte():
    quantidade: int
    padrao: np.ndarray


@dataclass
class Cortes():
    padroes: list[Corte]
    comprimentos: np.ndarray

    def __repr__(self):
        separador = "\n"
        cortes = [
            (
                f"Fazer {padrao.quantidade} vez(es) o(s) corte(s):"
                f" {padrao.padrao * self.comprimentos}"
            )
            for padrao in self.padroes
        ]

        return separador.join(cortes)


def principal(
    usos: np.ndarray,
    demandas: np.ndarray,
    nome="Problema da Mochila 0/1 com geração de colunas",
) -> gp.Model:
    """
    Define a modelagem do problema principal.

    Adiciona variáveis de decisão e restrições,
    além de definir a função objetivo.

    Args:
        usos (np.ndarray): Usos dos padrões de peças por peça inteira.
        demandas (np.ndarray): Demandas por peça.
        nome (str, optional): Nome do modelo.
            Por padrão: "Problema da Mochila 0/1 com geração de colunas".

    Returns:
        gp.Model: Modelo configurado.
    """
    modelo = gp.Model(nome)

    # Total de colunas
    total_colunas = len(demandas)
    quantidades = modelo.addMVar(total_colunas)

    modelo.setObjective(
        quantidades.sum()
    )

    modelo.addConstr(usos @ quantidades >= demandas)
    modelo.addConstr(quantidades >= 0.0)

    return modelo


def dual(
    comprimento: float,
    comprimentos: np.ndarray,
    duais: np.ndarray,
    nome="Dual do Problema da Mochila 0/1 com geração de colunas",
) -> gp.Model:
    """
    Define a modelagem do problema dual.

    Adiciona variáveis de decisão e restrições,
    além de definir a função objetivo.

    Args:
        comprimento (float): Comprimento da peça inteira.
        comprimentos (np.ndarray): Comprimentos por peça da demanda.
        duais (np.ndarray): Variáveis duais do problema principal.
        nome (str, optional): Nome do modelo.
            Por padrão: "Dual do Problema da Mochila 0/1 com geração de colunas".

    Returns:
        gp.Model: Modelo configurado.
    """
    modelo = gp.Model(nome)

    total_pecas = len(comprimentos)

    quantidades = modelo.addMVar(total_pecas, vtype=gp.GRB.INTEGER)

    modelo.setObjective(
        1 - duais @ quantidades
    )

    modelo.addConstr(comprimentos @ quantidades <= comprimento)

    return modelo


def resultados(modelo: gp.Model, comprimentos: np.ndarray, usos: np.ndarray) -> Cortes:
    """
    Define os padrões de cortes.

    Args:
        modelo (gp.Model): Modelo de otimização.
        comprimentos (np.ndarray): Comprimentos por peça da demanda.
        usos (np.ndarray): Padrões de usos das peças inteiras.

    Returns:
        Cortes: Padrões de corte.
    """
    quantidades = modelo.X

    cortes = Cortes(
        padroes=[
            Corte(
                quantidade=int(quantidade),
                # Cada coluna define um padrão
                padrao=usos[:, indice],
            )
            for indice, quantidade in enumerate(quantidades)
            if quantidade > 0
        ],
        comprimentos=comprimentos
    )

    return cortes


def exibir(modelo: gp.Model, cortes: Cortes):

    objetivo = modelo.objVal
    nome = modelo.ModelName

    print(f"\n\nProblema {nome}\n")
    print("Modelagem:")
    modelo.display()

    print(f"\nResultados:")
    print(cortes)

    print(f"\nUsar um total de {objetivo} peça(s) inteira(s).")


def otimizacao():
    """
    Define os dados de entrada, chamas as funções de processamento
    e exibi o resultado.
    """
    # Estoque de peças inteiras
    estoque = 10

    # Comprimento da peça inteira
    comprimento = 250

    comprimentos = np.array([187, 119, 74, 90])
    total_pecas = len(comprimentos)

    demandas = np.array([1, 2, 2, 1])

    # Padrões de usos iniciais
    # com uma unidade de demanda por peça inteira
    usos = np.identity(total_pecas)

    modelo = principal(usos, demandas)

    while True:
        modelo.optimize()

        duais = np.array(
            [
                restricao.Pi
                for restricao in modelo.getConstrs()
                if restricao.Pi > 0
            ]
        )

        submodelo = dual(comprimento, comprimentos, duais)
        submodelo.optimize()

        if submodelo.ObjVal >= 0:
            break

        # Novo padrao
        padrao = np.array(submodelo.X)

        # Atualiza usos com a nova coluna
        usos = np.column_stack((usos, padrao))

        restricoes = modelo.getConstrs()

        coluna = gp.Column(
            coeffs=padrao,
            constrs=restricoes[:total_pecas]
        )

        # Nova variável com coeficiente 1
        modelo.addVar(obj=1, column=coluna)

        modelo.update()

    cortes = resultados(modelo, comprimentos, usos)

    exibir(modelo, cortes)


if __name__ == "__main__":
    otimizacao()
