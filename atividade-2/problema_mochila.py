"""
Problema da mochila 0/1

Dados alguns itens distrintos com valor e peso específicos,
e uma mochila de capacidade máxima.

O objetivo é selecionar itens para colocar na mochila para obter o maior valor,
respeitando a capacidade da mochila.

Os itens apresentam somente dois estados: selecionado "1" ou não selecionado"'0".
"""

import numpy as np
import gurobipy as gp


def modelagem(
    capacidade: float,
    pesos: np.ndarray,
    valores: np.ndarray,
    nome="Problema da Mochila 0/1",
) -> gp.Model:
    """
    Define a modelagem do problema.

    Adiciona variáveis de decisão e restrições,
    além de definir a função objetivo.

    Args:
        capacidade (float): Capacidade da mochila.
        pesos (np.ndarray): Pesos por item.
        valores (np.ndarray): Valores por item.
        nome (str, optional): Nome do modelo.
            Por padrão: "Problema da Mochila 0/1".

    Returns:
        gp.Model: Modelo configurado.
    """
    modelo = gp.Model(nome)

    total_itens = len(pesos)
    itens = modelo.addMVar(total_itens, vtype=gp.GRB.BINARY)

    modelo.setObjective(itens @ valores, sense=gp.GRB.MAXIMIZE)

    restricoes = modelo.addConstr(itens @ pesos <= capacidade)

    return modelo


def resultados(modelo: gp.Model, capacidade: float) -> np.ndarray:
    """
    Calcula a ocupação das máquinas em porcentagem.

    Args:
        modelo (gp.Model): Modelo de otimização.
        capacidade (np.ndarray): Capacidade da mochila.

    Returns:
        np.ndarray: Ocupação percentual do mochila.
    """
    ocupacao = 0

    if len(modelo.Slack) > 0:
        ociosidade = modelo.Slack[0]
        ocupacao = (capacidade - ociosidade) / capacidade * 100

    return ocupacao


def exibir(modelo: gp.Model, ocupacao: np.ndarray):

    itens = modelo.X
    objetivo = modelo.objVal
    nome = modelo.ModelName

    print(f"\n\nProblema {nome}\n")
    print("Modelagem:")
    modelo.display()

    print(f"\nResultados:")

    print("Selecionar")
    for item, unidades in enumerate(itens):
        print(f"{unidades} unidades do item {item}")

    print("\nUsar")
    print(f"{ocupacao} % da mochila")

    print(f"\nAtingir um valor total de: {objetivo}")


def principal():
    """
    Define os dados de entrada, chamas as funções de processamento
    e exibi o resultado.
    """
    capacidade = 1000

    pesos = np.array(
        [
            65,
            94,
            119,
            59,
            149,
            114,
            57,
            136,
            100,
            150,
            122,
            117,
            120,
            130,
            133,
        ]
    )

    valores = np.array(
        [
            455,
            691,
            833,
            425,
            1064,
            758,
            419,
            914,
            651,
            966,
            828,
            827,
            857,
            837,
            894,
        ]
    )

    modelo = modelagem(capacidade, pesos, valores)

    modelo.optimize()

    ocupacao = resultados(modelo, capacidade)

    exibir(modelo, ocupacao)


if __name__ == "__main__":
    principal()
